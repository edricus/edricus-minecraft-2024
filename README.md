# edricus-minecraft-2024
Server SMP vanilla-friendly orienté exploration avec un datapack d'exploration spaciale et le mod "Sparse Structure" pour éviter de croiser un village tous les 100 blocs.  

## Details

<table>
  <tr>
    <th>IP</th>
    <td>minecraft.edricloud.org</td>
  </tr>
  <tr>
    <th>Version</th>
    <td>1.20.1</td>
  </tr>
  <tr>
    <th>Loader</th>
    <td>Fabric</td>
  </tr>
  <tr>
    <th>Port</th>
    <td>Par défaut</td>
  </tr>
  <tr>
    <th>Mods client-side</th>
    <td>Aucun</td>
  </tr>

</table>



## Mods server-side

### Sparse Structures  
![](https://cdn.modrinth.com/data/qwvI41y9/d43aa4806e99283b3bcee05005f7cdc86b63e59e.png){width=100}  
A simple and configurable one-mixin mod that makes all (even datapacks and modded) structures more spread out (or more common!), essentially making them rarer/easier to find. Useful in big modpacks with a lot of structures mods to encourage exploration  
https://modrinth.com/mod/sparsestructures  
### Dungeons and Taverns  
![](https://cdn.modrinth.com/data/tpehi7ww/d3819bdb14026e23a9d48b0cc0be99a1a25ea2b5.png){width=100}  
A Structure Datapack adding dungeons, taverns and other structures to find while you explore the world.  
https://modrinth.com/datapack/dungeons-and-taverns  
### The Expension  
![](https://cdn.modrinth.com/data/t3qZik6A/e724249203b9ef4e89c704e1fcd80fa2988f10ff.png){width=100}  
Explore Space In Minecraft!  
https://modrinth.com/datapack/the-expansion  
### Fabric Seasons
![](https://cdn.modrinth.com/data/KJe6y9Eu/7ce47fcb4242493ad069789298400b14f27737ad.png){width=100}  
A simple mod that adds seasons to the game, dynamically changing biomes as you play.  
https://modrinth.com/mod/fabric-seasons  
### Geophilic
![](https://cdn.modrinth.com/data/hl5OLM95/0a857b9f9e96f9f09fe6a32868be3bbb07f1fed1.jpeg){width=100}  
A subtle overhaul of vanilla Overworld biomes  
https://modrinth.com/datapack/geophilic  
### Waystones (Datapack)
![](https://cdn.modrinth.com/data/LOpKHB2A/icon.png){width=100}  
Teleport back to activated waystones. For Survival, Adventure or Servers.  
https://www.planetminecraft.com/data-pack/waystones/  
### SleepWarp
![](https://cdn.modrinth.com/data/OPvzuqtZ/icon.png){width=100}  
Accelerates time when sleeping instead of skipping directly to day.  
https://modrinth.com/mod/sleep-warp  
### Just Player Heads
![](https://cdn.modrinth.com/data/YdVBZMNR/icon.jpg){width=100}  
🗿 Allows the collection of player heads via command or death event.  
https://modrinth.com/mod/just-player-heads  
### Your Items Are Safe  
![](https://cdn.modrinth.com/data/lL35xmSR/icon.jpg){width=100}  
💀 Spawns a chest and armor stand with player items on death, a gravestone alternative.  
https://modrinth.com/mod/your-items-are-safe  
### Right Click Harvest
![](https://cdn.modrinth.com/data/Cnejf5xM/f79a0166a6572525079c2835d8ed0980b11bfbc9.png){width=100}  
Allows you to harvest crops by right clicking  
https://modrinth.com/mod/rightclickharvest  
### Dismount Entity
![](https://cdn.modrinth.com/data/H7N61Wcl/icon.png){width=100}  
🚏 Allows players to dismount/remove/exit entities/mobs from mounted entities without breaking them.  
### Animated Doors
![](https://cdn.modrinth.com/data/EuloLapn/a0fafffd1e0f9a84c5f842a4b8d22b5696275b0c.png){width=100}  
Door open & close animation. Connects double doors to open simultaneously. Supports iron doors and redstone signals  
https://modrinth.com/datapack/animated-doors  
### Grass Seeds
![](https://cdn.modrinth.com/data/Y6d4uRJn/icon.png){width=100}  
🌱 Allows players to use normal seeds to transform dirt into (tall) grass.  
https://modrinth.com/mod/grass-seeds  
### Skeleton Horse Spawn
![](https://cdn.modrinth.com/data/ZcqNoW8j/icon.png){width=100}  
☠🐴 Allows skeleton/zombie horses to spawn naturally with a skeleton riding it and other tweaks.  
https://modrinth.com/mod/skeleton-horse-spawn    
### Scaffolding Drops Nearby
![](https://cdn.modrinth.com/data/uO522mgw/icon.png){width=100}  
🏗 When breaking a scaffolding block, all chained blocks will drop at the block's position.  
https://modrinth.com/mod/scaffolding-drops-nearby  
### Followers Teleport Too
![](https://cdn.modrinth.com/data/E5YVNsbH/0b57b748e61a3ca42556eaf2802b624c6cd6e9ee.png){width=100}  
🐺 Whenever a player is teleported via a command, their pets will be teleported alongside them.  
https://modrinth.com/mod/followers-teleport-too  
### Random Bone Meal Flowers
![](https://cdn.modrinth.com/data/17enPZMC/icon.png){width=100}  
🎲 Randomizes the flowers spawned by bonemeal, allowing all (modded) types to spawn everywhere.  
https://modrinth.com/mod/random-bone-meal-flowers  
### Village Bell Recipe  
![](https://cdn.modrinth.com/data/u692zai1/icon.png){width=100}  
🔔 Makes the village bell item craftable via a recipe.
https://modrinth.com/mod/village-bell-recipe  
### CraftableNameTag
![](https://cdn.modrinth.com/data/mi57UBwo/d087aad208bf88342f8bd516a70937e608f74b52.png){width=100}  
https://modrinth.com/datapack/craftable-name-tag  
### Craftable Saddle
![](https://cdn.modrinth.com/data/PamPmbXh/fff9cc53983d5dc5d2ac054d00d5dc29a63be659.png){width=100}  
Makes Saddles craftable (9/10 rideable mobs recommends)  
https://modrinth.com/datapack/piseks-craftable-saddle 
### Axes Are Weapons
![](https://cdn.modrinth.com/data/1jvt7RTc/icon.png){width=100}  
Disables the increased durability loss in combat and enables Looting for axes and more!  
https://modrinth.com/mod/axes-are-weapons  
### Companion
![](https://cdn.modrinth.com/data/4w0EzGRW/bf3213bc6a0ad5570d040d5d57672a952a29cae6.gif){width=100}  
Mechanics to avoid losing your pets, but do not break the vanilla feel  
https://modrinth.com/mod/companion  
### Hero Proof
![](https://cdn.modrinth.com/data/GDmztpJg/29a1e0ab7a4102aed10a3e6afef3c12cc9e73a7c.png){width=100}  
Earn a medal after winning a raid, and remind villagers to remember you as a hero!  
https://modrinth.com/datapack/hero-proof  
### Invisible Frames
![](https://cdn.modrinth.com/data/QD87oMUf/55da1702b1835d0cfe3c6a01b867142731a93b6d.png){width=100}  
Easily toggle invisibility on your item frames by sneaking.  
https://modrinth.com/mod/invisible-frames-mod  
### Villager Transportation
![](https://cdn.modrinth.com/data/vLUPqRLH/7b276f3cc79620281ee58feeefe34508a46631aa.png){width=100}  
Easier Villager transportation by using Camels and Llamas!    
https://modrinth.com/datapack/villager-transportation
